import { BrowserRouter, Routes, Route, Link, Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import HomePage from './pages/HomePage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import NotFoundPage from './pages/NotFoundPage';
import { RootState } from './store';

function App() {

    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);

    return (
        <BrowserRouter>
        <header>
            <nav>
                <ul>                    
                    <li>
                        <Link to="/">Главная</Link>
                    </li>
                    <li>
                        <Link to="/register">Регистрация</Link>
                    </li>
                    <li>
                        <Link to="*">404</Link>
                    </li>
                </ul>
            </nav>
        </header>

        <main>
            <Routes>
                 <Route path="/" element={!isAuthenticated ? <Navigate to="/login" /> : <HomePage />} />
                 <Route path="/login" element={isAuthenticated ? <Navigate to="/" /> : <LoginPage />} />
                 <Route path="/register" element={isAuthenticated ? <Navigate to="/" /> : <RegisterPage />} />
                 <Route path="*" element={<NotFoundPage />} />
            </Routes>
        </main>
    </BrowserRouter>
    );
}

export default App;