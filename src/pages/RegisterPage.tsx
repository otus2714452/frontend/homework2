import React, { useState } from 'react';
import { Button, Container, CssBaseline, TextField, Typography } from '@mui/material';
import { connect } from 'react-redux';
import { registerAction } from '../components/Actions';

interface FormStyles {
    paper: React.CSSProperties;
    form: React.CSSProperties;
    submit: React.CSSProperties;
}

interface RegisterProps {
    register: (username: string) => void;
}

function Register({
    register
}: RegisterProps) {

    const [formData, setFormData] = useState({
        username: '',
        password: '',
        confirmPassword: ''
    });

    const [passwordError, setPasswordError] = useState('');

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const { username, password, confirmPassword } = formData;

        setPasswordError('');

        if (password !== confirmPassword) {
            setPasswordError('Пароль и подтверждение пароля не совпадают');
            return;
        }

        register(username);
    };

    return (
        <Container component="main">
            <CssBaseline />
            <div>
                <Typography>Регистрация</Typography>
                <form onSubmit={handleSubmit}>
                    <div>
                        <TextField label="Имя пользователя"
                            name="username"
                            value={formData.username}
                            onChange={handleChange} />
                    </div>
                    <div>
                        <TextField label="Пароль"
                            type="password"
                            name="password"
                            value={formData.password}
                            onChange={handleChange}
                            required />
                    </div>
                    <div>
                        <TextField label="Подтверждение пароля"
                            type="password"
                            name="confirmPassword"
                            value={formData.confirmPassword}
                            onChange={handleChange} />
                    </div>
                    <div>
                        <Button type="submit"
                            variant="contained"
                            color="primary"> Зарегистрироваться </Button>
                    </div>
                </form>
            </div>
        </Container >
    );
}

const mapDispatchToProps = {
    register: registerAction
};

export default connect(null, mapDispatchToProps)(Register);
