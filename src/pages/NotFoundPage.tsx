import { Container, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

const NotFound = () => {
  return (
    <Container>
      <Typography>404 Not found</Typography>
    </Container>
  );
};

export default NotFound;