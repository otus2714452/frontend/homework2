import { Typography, Container, Button } from '@mui/material';
import { connect } from 'react-redux';
import { RootState } from '../store';
import { logoutAction } from '../components/Actions';
import { Link } from 'react-router-dom';

interface HomePageProps {
    username: string | undefined,
    logout: () => void;
}

function HomePage({ username, logout }: HomePageProps) {
    return (
        <Container>
            <div>
                <Typography> Вход для {username} успешно выполнен </Typography>
                <Button onClick={logout}> ВЫЙТИ </Button>                
            </div>
        </Container>
    );
};

const mapStateToProps = (state: RootState) => ({
    username: state.auth.user?.username,
});

const mapDispatchToProps = {
    logout: logoutAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);